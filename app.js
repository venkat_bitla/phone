const express = require('express')
const path = require('path');
const app = express()
const port = 8080
const parsePhoneNumber = require('libphonenumber-js')

app.use(express.static(path.join(__dirname, 'public')));

app.get('/phone/:number?', function(req, res){
  let phoneNum = req.params.number;
  let phones = (phoneNumber) => {
    const phonePad = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"] 
    let lastPhoneDigit = phoneNumber.charAt(phoneNumber.length-1)
    let padAlpha = phonePad[lastPhoneDigit]
    let result = [];
    for (let letter of padAlpha) {
      let modifyPhone = phoneNumber.slice(0, -1) + letter.toUpperCase()
      result.push(modifyPhone)
    }
    return result
  }
  const phoneList = phones(phoneNum)
  const phoneCount = phoneList.length
  const respData = { 'phones': phoneList, 'size': phoneCount} 
  res.send(respData);
});

app.get('/client.js', function(req, res){
  res.sendFile(path.join(__dirname, 'client.js'));
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})

module.exports = app