var phoneContainer = document.querySelector('[name=phone]');
var button = document.querySelector('#btn-submit');
var code = document.querySelector('pre');

button.addEventListener('click', function(){
  const phoneVal = phoneContainer.value
  // console.log(isNaN(Number(phoneVal)))
  // console.log(phoneVal.length)
  if (isNaN(Number(phoneVal))) {
   alert("Please enter valid phone number.");
   return false
  } else if(phoneVal.length != 7 && phoneVal.length != 10){
    alert("Please enter valid phone number length.");
    return false
  }
  var xhr = new XMLHttpRequest;
  xhr.open('GET', '/phone/' + phoneVal, true);
  xhr.onreadystatechange = function(){
    if (xhr.readyState === 4) {
      code.textContent = xhr.responseText;
    }
  };
  
  xhr.send();
}, false);