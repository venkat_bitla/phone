var assert = require("assert");
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("./app");
let should = chai.should();


chai.use(chaiHttp);

describe("Phones", function(){
  it("should get phones", done=>{
    chai.request(server).get("/phone/9000525629")
    .end((err, result)=>{
        result.should.have.status(200);
        console.log ("Got", result.body, " docs")
        done()
    })
  })
})